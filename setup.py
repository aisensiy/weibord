from setuptools import setup

requires = [

    'Paste',
    'decorator',
    'jinja2',
    'rauth',
    'redis',
    'simplejson',
    'web.py',
    'requests',

    ]

setup(name='wclient',
      version='0.1',
      author='Xiaojiang Shen',
      author_email='blog@siteshen.com',
      install_requires=requires)
