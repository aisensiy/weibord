(function() {
  var $ = window.jQuery;                // for js2-mode warning

  $('.img-thumb').live('click', function() {
    var $this = $(this);
    var $status = $(this).closest('.status');
    var $c_image = $this.closest('.c-image');
    var $thumb = $('.img-thumb', $c_image);
    var $middle = $('.img-middle', $c_image);
    var $c_text = $('.c-text', $status);
    var width_diff = $middle.width() - 120;
    var width_text = $c_text.width();
    $thumb.hide();
    $middle.show();
    $c_text.width(width_text - width_diff);
  });

  $('.img-middle').live('click', function() {
    var $this = $(this);
    var $status = $(this).closest('.status');
    var $c_image = $this.closest('.c-image');
    var $thumb = $('.img-thumb', $c_image);
    var $middle = $('.img-middle', $c_image);
    var $c_text = $('.c-text', $status);
    var width_diff = $middle.width() - 120;
    var width_text = $c_text.width();
    $thumb.show();
    $middle.hide();
    $c_text.width(width_text + width_diff);
  });

  $('.c-comment').live('click', function(e) {
    $('.repost-box').hide();
    var $box = $(e.currentTarget).siblings('.comment-box');
    $box.toggle();
  });

  $('.do-comment').live('click', function(e) {
    var $this = $(this);
    var $text = $this.siblings('input[name=text]');
    var $status = $(this).closest('.status');
    var data = {
      comment: $.trim($text.val()),
      id: $status.attr('data-id')
    };

    $.post('/comments/create', data, function(resp) {
      if (resp.status === 'success') {
        $text.val('');
        $this.closest('.comment-box').hide();
      } else {
        alert(resp.message);
        $text.focus();
      }
    });
  });

  $('.c-repost').live('click', function(e) {
    $('.comment-box').hide();
    var $box = $(e.currentTarget).siblings('.repost-box');
    $box.toggle();
  });

  $('.do-repost').live('click', function(e) {
    var $this = $(this);
    var $text = $this.siblings('input[name=text]');
    var $status = $(this).closest('.status');
    var data = {
      status: $.trim($text.val()),
      id: $status.attr('data-id')
    };

    $.post('/statuses/repost', data, function(resp) {
      if (resp.status === 'success') {
        $text.val('');
        $this.closest('.repost-box').hide();
      } else {
        alert(resp.message);
        $text.focus();
      }
    });
  });

})();
